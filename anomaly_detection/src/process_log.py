from collections import defaultdict, deque
import sys
import json
import json_utils
import networkx as nx
import pandas as pd
import numpy as np
import datetime

def read_input(batch_log_path, stream_log_path):
    # Note: The input files are not in the standard json format.
    def to_valid_json(f):
        # Read log line by line and avoid any spaces
        lines = filter(lambda l: l != '', [l.strip() for l in f.readlines()])
        # return a json array
        return '[' + ',\n'.join(map(lambda x: x.strip(), lines)) + ']'

    def pre_process_log(f):
        # Data Frames using Pandas
        df = pd.read_json(f)
        for col in filter(lambda x: x in df.columns, {'id', 'id1', 'id2'}):
            # If a column has NaN replace it with -1
            df[col].fillna(-1, inplace=True)
            # Change id, id1 and id2 to integer type
            df[col] = df[col].astype(int)
        return df
    # Instantiate an empty dictionary
    res = {}
    # Open batch and stream log files and close them after parsing
    with open(batch_log_path) as batch_log, open(stream_log_path) as stream_log:
        # Read T and D (first line)
        td = json.loads(batch_log.readline())
        td = {k: int(v) for k, v in td.items()}
        res.update(td)

        # read the rest of the batch file (friend, unfriend, etc.)
        res['batch_log'] = pre_process_log(to_valid_json(batch_log))

        # read the stream file
        res['stream_log'] = pre_process_log(to_valid_json(stream_log))
    return res


# Class for detecting anomaly in purchase
class AnomalyDetection:
    # Initiate the class with given input and output paths
    def __init__(self, batch_log_path, stream_log_path, flagged_purchases_path):
        # Use the helper functions to read the input files
        data = read_input(batch_log_path, stream_log_path)
        # Store number of consecutive purchases and the degree
        self.T = data['T']
        self.D = data['D']
        # Store batch and stream from input files
        self.batch_log = data['batch_log']
        self.stream_log = data['stream_log']
        # Add index of batch to index of stream
        self.stream_log.index = self.stream_log.index + self.batch_log.shape[0]
        self.flagged_log = flagged_purchases_path
        # TODO should check data is sorted on timestamp && batch_log < stream_log
        # Construct the social network
        self.social_network = self._construct_init_sn()
        #
        self.purchase_q, self.count_q = self._construct_init_qq()

    # Helper function to construct the social network
    def _construct_init_sn(self):
        log = self.batch_log
        # find out the edges to add
        friend = set(
            tuple(x) for x in log[log.event_type == 'befriend'][['id1', 'id2']].to_records(index=False))
        unfriend = set(
            tuple(x) for x in log[log.event_type == 'unfriend'][['id1', 'id2']].to_records(index=False))

        sn = nx.Graph()
        sn.add_edges_from(friend)
        sn.remove_edges_from(unfriend)
        return sn
    # Initialize the history of purchases
    def _construct_init_qq(self):
        T = self.T
        log = self.batch_log
        #### ? ####
        # Initialize a purchase dictionary with length T
        # that contains index of event and the amount for each user
        purchase_q = defaultdict(lambda: deque([(-1, 0)] * T, T))
        # Number of purchases made by each user
        count_q = defaultdict(int)
        for e in log[log.event_type == 'purchase'][['id', 'amount', 'timestamp']].itertuples():

            purchase_q[e.id].appendleft((e.Index, e.amount))
            count_q[e.id] += 1
        return purchase_q, count_q

    def process_stream(self):
        for e in self.stream_log.itertuples():
            self.process_event(e)
        pass

    def process_event(self, e):
        if e.event_type == 'purchase':
            self.purchase(e)
        elif e.event_type == 'befriend':
            self.befriend(e)
        elif e.event_type == 'unfriend':
            self.unfriend(e)
        else:
            raise ValueError('Event Type Unknown')

    def purchase(self, e):
        self.raise_if_anomalous(e)
        # Q: What happens if events for a particular id exceeds T?
        self.purchase_q[e.id].appendleft((e.Index, e.amount))

    def befriend(self, e):
        self.social_network.add_edge(e.id1, e.id2)

    def unfriend(self, e):
        self.social_network.remove_edge(e.id1, e.id2)

    def raise_if_anomalous(self, e):
        if self.is_anomalous(e)[0]:
            self.raise_flag(e, self.is_anomalous(e)[1:])

    def raise_flag(self, e, stat):
        self.dump_anomaly(e,stat)
    
    # Dump the flagged purchase to file
    def dump_anomaly(self, e, stat):
        dic = {}
        dic['event_type'] = str(e[2])
        dic['timestamp']  = str(e[4])
        dic['id']         = str(e[3])
        dic['amount']     = str(e[1])
        dic['mean']       = str("%.2f" % stat[0])
        dic['sd']         = str("%.2f" % stat[1])
        with open(self.flagged_log, 'w') as f:
            json.dump(dic, f)

    def is_anomalous(self, e):
        # single_source_shortest_path_length(graph, source, cutoff)
        d_nodes = set(n for (n, d) in nx.single_source_shortest_path_length(
                                self.social_network, e.id, self.D).items()) - {e.id}

        # Store all purchases made within the specified connectivity of a user
        # without including the user itself
        purchase_qs = [iter(self.purchase_q[n]) for n in d_nodes]
        latest_purchases = []
        while len(latest_purchases) < self.T:
            candidates = [next(q) for q in purchase_qs]
            latest_purchase = max(candidates)

            if latest_purchase[0] == -1:
                break
            latest_purchases.append(latest_purchase[1])
        mean = np.mean(latest_purchases)
        sd   = np.std(latest_purchases)
        if len(latest_purchases) >= 2 and e.amount > mean + 3 * sd:
            return True, mean, sd
        else:
            return False, 0, 0

def main(argv):
    print(argv)
    batch_log_path  = argv[1]
    stream_log_path = argv[2]
    flagged_purchases_path = argv[3]
    anomaly_detection = AnomalyDetection(batch_log_path, stream_log_path, flagged_purchases_path)
    anomaly_detection.process_stream()
    pass


if __name__ == '__main__':
    main(sys.argv)

