import os, sys
import json

def load_data(fid):
    data = []
    with open(os.path.join(fid)) as f:
        for line in f:
            data.append(json.loads(line))
    print "Loaded file: "+fid
    return data;

def main(argv):
    
    batch_data  = load_data(argv[0])
    stream_data = load_data(argv[1])

    return;

if __name__ == "__main__":
   main(sys.argv[1:])
