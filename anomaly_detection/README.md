# README #

The is Python code to build a real-time platform to analyze purchases within a social network of users at Market-ter, and detect any behavior that is far from the average within that social network.

Version: 1.0

### How do I get set up?

Python 3.X is required.
Required libraries need to be installed before running the program.

json: To read input files containing dictionaries 

networkx: A graph library to easily calculate connectivities and so on.

pandas: A fast and flexble data structure


### Who do I talk to?

Owner: Benyamin Gholami 
Email: bgholam1@asu.edu
